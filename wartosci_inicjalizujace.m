%% Parametry symulacji
%skala_czasu=1;
%skala_czasu = 1/45;       % jednostka czasu symulacji = 1 sek czasu rzeczywistego
skala_czasu = 1/60;     % jednostka czasu symulacji = 1 min czasu rzeczywistego
%skala_czasu = 1/60/60; % jednostka czasu symulacji = 1 godz czasu rzeczywistego
ile_dni=5;             % liczba symulowanych dni
Tsym=(60*60*24)*ile_dni*skala_czasu;
tp = skala_czasu*10; % czas pr�bkownia

%% Parametry aparatury pomiarowej
sampletime=10000*skala_czasu.^(-1);%czas pr�bkowania ADC
rozdzielczosc=2^12;
Tmin=-55;%parametry dla czujnika temperatury ds18b20
Tzak=125;
%% Wymiary magazynu
H_pom=5; % [m]
W_pom=20;   % [m]
L_pom=35;   % [m]
gr_scian=0.1; % grubo�� �cian [m]                                                       
gest_waty=    70; %[kg/m3]
Pow_scian=(H_pom*(W_pom+L_pom)*2)+(W_pom*L_pom); % powierzchnia scian+dach 
V_scian=Pow_scian*gr_scian; % ob�to�� �cian
V_pow=H_pom*W_pom*L_pom; % objetosc pomieszczenia
  
%% Wsp�czynniki przewodnictwa cieplnego (?)
% [W/m*K] = [kg*m/s3*K]
wpc_w=0.63*skala_czasu.^(-3); % wsp�czynnik przewodnictwa cieplnego wody
wpc_cu=380*skala_czasu.^(-3); % wspo�czynnik przewodnictwa cieplnego miedzi
wpc_ze=50*skala_czasu.^(-3); % wspo�czynnik przewodnictwa cieplnego �eliwa
wpc_pow=0.025*skala_czasu.^(-3); %wsp�czynnik przewodnictwa cieplnego powietrza
wpc_scian=0.6*skala_czasu.^(-3); % wsp�czynnik przewodnictwa cieplnego �cian
wpc_welny_mineralnej=0.035*skala_czasu.^(-3); %wsp�czynnik przewodnictwa cieplnego �cian

%% Temperatury pocz�tkowe 
T_pocz_pow_zew=30; % pocz�tkowa temperatura na dworze
T_pocz_pow=6; % pocz�tkowa temperatura powietrza w magazynie
T_pocz_nagrzewnicy=-10;%pocz�tkowa temperatura nagrzewnicy
T_pocz_scian=(T_pocz_pow_zew+T_pocz_pow)/2; % pocz�tkowa temperatura scian
T_pocz_palety=25;
%% Wsp�czynniki ciep�a w�a�ciwego [J/kg*K] = [m/s2*K]
CW_wody=4189.9*skala_czasu.^(-2); %ciep�o w�as�iwe wody
CW_cu=386*skala_czasu.^(-2); %ciep�o w�as�iwe cu
CW_ze=440*skala_czasu.^(-2); %ciep�o w�as�iwe cu
CW_pow=1005*skala_czasu.^(-2); %ciep�o w�a�ciwe powietrza
CW_scian=880*skala_czasu.^(-2); %cieplo w�a�ciwe �cian
CW_szkla=800*skala_czasu.^(-2);%ciep�o w�a�ciwe szk�a(butelek)
CW_welny_mineralnej=800*skala_czasu.^(-2);%ciep�o w�a�ciwe we�ny mineralnej

%% Masy wp�ywaj�ce na pojemno�� ciepln� uk�adu
m_scian=V_scian*gest_waty; %masa �cian
gest_p=1.168; % g�sto�� powietrza
m_pow=V_pow*gest_p; %masa powietrza


%%-warto�ci ponizej nalezy dok�adnie zweryfikowac,te u g�ry wydaj� sie
%%sensowne


%% Wsp�czynnik przenikania ciep�a [W/m2*K]:
R_scian=gr_scian/wpc_scian; % op�r cieplny �cian
%R_palety=0.0000006;
R_palety=0.001/wpc_w;

%% Wsp�czynnik przejmowania ciep�a przegrody powietrze inny materia� przy swobodnym przep�ywie powietrza
alfa=8.91*skala_czasu.^(-3); % wsp�czynnik przejmowania ciep�a
RT=1/alfa;   % op�r przejmowania ciepla
RT2=RT*2;
% wariant 2
Rsi=0.13*skala_czasu.^(3);
Rse=0.04*skala_czasu.^(3);
RT2=Rsi+Rse; % op�r przejmowania ciepla

%% Parametry wstawianego obiektu(tymbarka)
tymbarki_na_palete = 1848;
paleta_w =     1.200;     %[m]
paleta_l =     0.800;    %[m]
paleta_h =   1.379;     %[m]
paleta_v = paleta_w * paleta_l * paleta_h;
Pow_palety=2*paleta_l*paleta_h+2*paleta_w*paleta_h+paleta_l*paleta_w;%Powierzchnia palety
Masa_butelki=0.17;%kg
m_palety= 779;
CW_palety=CW_wody;
m_pow_1_paleta=paleta_v*gest_p;%masa powietrza wypartego przez jedn� palet�

%% Nagrzewnica
P_nag=100000*skala_czasu.^(-3);%moc nagrzewnicy [W=(kg*(m/s2))/s]
Pow_nag=20;
d_nag=7700;
m_nag=d_nag*Pow_nag*0.01;

R_ze_nag=0.1/wpc_cu;

%% Nowe obliczenia do wentylatorow
Lepkosc_kin_pow = 17.08e-6/gest_p; % = lepkosc_dynamiczna / gestosc
Sr_went = 0.75; % Srednica wentylatora/rur wentylacyjnych [m]
%Re_pow = predkosc przep�ywu*skala_czasu.^(-1) * Sr_went / Lepkosc_kin_pow
Prandtl_pow = 0.7;
%Nu_pow = 0.023 / Sr_went * Prandtl_pow.^0.4 * (Sr_went / Lepkosc_kin_pow).^0.8 * wpc_pow
Const_wentylatorow = 0.023 * wpc_pow * Prandtl_pow.^0.4 * (Sr_went / Lepkosc_kin_pow).^0.8;
v_max_went = 5 * skala_czasu.^(-1);
%% inne obliczenia przep�ywu
wydajnosc=3300;%kg/min
fan_rpm=1500
fan_displ=(3300/gest_p)/1500
fan_flow=fan_rpm/(fan_displ/60)*CW_pow
